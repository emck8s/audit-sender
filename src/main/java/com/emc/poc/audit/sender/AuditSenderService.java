package com.emc.poc.audit.sender;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.emc.poc.audit.sender.model.AuditMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Random;

@Slf4j
@Component
public class AuditSenderService {

    private final String queueName;
    private final AmazonSQS sqs;
    private final String queueUrl;

    public AuditSenderService(@Value("${queue.name}") final String queueName) {
        this.queueName = queueName;
        sqs = AmazonSQSClientBuilder.defaultClient();
        queueUrl = sqs.getQueueUrl(queueName).getQueueUrl();
        log.info("queueUrl = {}", queueUrl);
    }

    public void sendAudit() {
        int randomNumber = new Random().nextInt(50) + 1;
        AuditMessage message = new AuditMessage(randomNumber, "source" + String.valueOf(randomNumber), "some content");

        SendMessageRequest send_msg_request = new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(message.toString())
                .withDelaySeconds(5);
        sqs.sendMessage(send_msg_request);
    }
}
