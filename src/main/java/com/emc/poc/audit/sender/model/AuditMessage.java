package com.emc.poc.audit.sender.model;

import com.amazonaws.services.sqs.model.Message;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AuditMessage extends Message {

    private int severity;
    private String source;
    private String content;

}
