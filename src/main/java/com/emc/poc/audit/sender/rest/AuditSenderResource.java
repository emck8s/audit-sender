package com.emc.poc.audit.sender.rest;


import com.emc.poc.audit.sender.AuditSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/send-audit")
public class AuditSenderResource {

    @Autowired
    private AuditSenderService auditService;

    @PostMapping
    public void sendAuditMessage() {
        auditService.sendAudit();
    }
}
