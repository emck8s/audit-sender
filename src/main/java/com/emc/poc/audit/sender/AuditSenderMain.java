package com.emc.poc.audit.sender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuditSenderMain {
    public static void main(String[] args) {
        SpringApplication.run(AuditSenderMain.class, args);
    }
}
